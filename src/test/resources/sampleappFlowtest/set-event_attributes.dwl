{
  "headers": {
    "user-agent": "PostmanRuntime/7.26.2",
    "accept": "*/*",
    "postman-token": "84dfddf7-9390-41cc-abc4-154c2e7683c7",
    "host": "localhost:8081",
    "accept-encoding": "gzip, deflate, br",
    "connection": "keep-alive"
  },
  "clientCertificate": null,
  "method": "GET",
  "scheme": "http",
  "queryParams": {},
  "requestUri": "/sample",
  "queryString": "",
  "version": "HTTP/1.1",
  "maskedRequestPath": null,
  "listenerPath": "/sample",
  "relativePath": "/sample",
  "localAddress": "/127.0.0.1:8081",
  "uriParams": {},
  "rawRequestUri": "/sample",
  "rawRequestPath": "/sample",
  "remoteAddress": "/127.0.0.1:62228",
  "requestPath": "/sample"
}